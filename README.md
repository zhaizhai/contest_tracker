# contest_tracker

### Setup

First, create a directory for all your contests. In this example,
we'll call it `my_practice_dir`.

```
$ cd ~
$ mkdir /my_practice_dir
```

Within this directory, there should be a subdirectory for each
contest. Let's make one called `2013-qual`, for the qualification
round for Google Codejam 2013.

```
$ mkdir /my_practice_dir/2013-qual
```

Now, we can run the main program:

```
$ ./stats.py
Please enter the path of the directory containing your soluton files: ~/my_practice_dir

You have no active contests with problems available.

>>
```

### Basic usage

Type `?` to see a list of all available commands. The most important
commands are `contest` and `prob`. Here's a typical workflow:

```
>> prob

suggest -- Suggest changes to problem statuses.
add -- Add a problem to the list of active contests.
list -- List active problems.
mark -- Mark a problem's status.

>> prob list

You have no active contests with problems available.

>> prob add

Enter contest name: 2013-qual
This contest is not currently in the database. Create a new contest? (y/N)y
Enter problem number: A
Enter problem name: tic-tac-tomek
>> contest list
    2013-qual
>> contest add 2013-qual
>> contest list
[ ] 2013-qual
>> prob list
------------------------------------------------------------
2013-qual  - [A - tic-tac-tomek - TODO]
------------------------------------------------------------
[x]
------------------------------------------------------------
>> prob mark tic-tac-tomek done
>> prob list
------------------------------------------------------------
2013-qual  - [A - tic-tac-tomek - DONE]
------------------------------------------------------------
[x]
------------------------------------------------------------
>>
```