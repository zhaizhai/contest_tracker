#!/usr/bin/python

from collections import defaultdict

import cmd
import json
import os
import shlex

from persistence.sqlite_store import Problem, Contest, ProblemStore
from command_suites.contest import ContestCommandSuite
from command_suites.prob import ProbCommandSuite
from command_suites.stats import StatsCommandSuite

class ProgressTracker(object):
    KNOWN_EXTENSIONS = ['py', 'cc', 'cpp']

    def __init__(self, root_dir):
        self.root_dir = root_dir
        if not os.path.exists(root_dir):
            print 'Warning: source directory %s does not exist' % root_dir

    def check_present(self, contest_name, prob):
        possible_names = [prob.name, prob.number]
        for name in possible_names:
            for ext in ProgressTracker.KNOWN_EXTENSIONS:
                sol_path = os.path.join(self.root_dir, contest_name, name) + '.' + ext
                if os.path.exists(sol_path):
                    return True
        return False

    def suggest_status(self, contest_name, prob):
        present = self.check_present(contest_name, prob)

        if prob.status == 'done' and not present:
            return 'missing'
        elif prob.status == 'todo' and present:
            return 'solving'
        elif prob.status == 'missing' and present:
            return 'done'
        elif (prob.status == 'solving' or prob.status == 'missing') and not present:
            return 'todo'
        return prob.status



# TODO: add a decorator to parse command line arguments
class Term(cmd.Cmd):
    def __init__(self, problem_store, config_manager):
        cmd.Cmd.__init__(self)
        self.current_path = ''
        self.prompt = ">> "

        self.cmd_running = False

        self.config_manager = config_manager
        self.problem_store = problem_store
        self.progress_tracker = ProgressTracker(self.config_manager.config['src_root'])

        self.contest_cmd = ContestCommandSuite(self, problem_store)
        self.stats_cmd = StatsCommandSuite(self, problem_store)
        self.problem_cmd = ProbCommandSuite(self, problem_store, self.progress_tracker)

    def contest_stats(self, contest_id):
        status_map = defaultdict(int)

        all_probs = self.problem_store.query_problems()
        probs = [prob for prob in all_probs if prob.contest_id == contest_id]
        for prob in probs:
            status_map[prob.status] += 1
        return status_map

    def contest_stats_aggregate(self, contest_ids):
        status_map = defaultdict(int)

        all_probs = self.problem_store.query_problems()
        probs = [prob for prob in all_probs if prob.contest_id in contest_ids]
        for prob in probs:
            status_map[prob.status] += 1
        return status_map

    def sort_rank(self, contest):
        ROUNDS = [['qual'], ['1a', '1b', '1c'], ['2'], ['3'],
                  ['amer', 'apac', 'emea'], ['final']]

        parts = contest.name.split('-')
        year, round_num = parts[0], parts[1]

        for i in xrange(len(ROUNDS)):
            if round_num in ROUNDS[i]:
                return (i, year)
            return (-1, year)

    def emptyline(self):
        return

    def default(self, line):
        print 'Unrecognized command %s' % line

    def precmd(self, line):
        self.cmd_running = True
        return line

    def postcmd(self, stop, line):
        self.cmd_running = False
        return stop

    # override to handle ctrl + c
    def cmdloop(self, initial=True):
        if initial:
            self.problem_cmd.list()
            self.problem_cmd.suggest('-s')

        try:
            cmd.Cmd.cmdloop(self)
        except KeyboardInterrupt as e:
            if not self.cmd_running:
                print ''
                self.do_exit('')
            else:
                print '^C'
                self.cmd_running = False
                self.cmdloop(initial=False)

    def do_exit(self, args):
        return True

    def run_cmd_suite(self, cmd_suite, line):
        args = [s for s in line.split() if s]
        cmd_map = cmd_suite.command_map()

        if len(args) == 0:
            if '' in cmd_map:
                command, _ = cmd_map['']
                command()
                return

            print
            for name, (command, desc) in cmd_map.iteritems():
                print name + ' -- ' + desc
            print
            return

        cmd_name = args[0]
        args = args[1:]
        if cmd_name not in cmd_map:
            print 'Unknown command %s' % cmd_name
            return

        command, _ = cmd_map[cmd_name]
        command(*args)

    # regular commands

    def do_import(self, args):
        args = [s for s in args.split() if s]
        file_name = args[0]
        try:
            self.problem_store.import_from_file(file_name)
        except KeyboardInterrupt:
            print 'Import aborted'
        except Exception, e:
            print 'Failed to import:', e

    def do_export(self, args):
        args = [s for s in args.split() if s]
        file_name = args[0]
        try:
            self.problem_store.export_to_file(file_name)
        except KeyboardInterrupt:
            print 'Export aborted'
        except Exception, e:
            print 'Failed to export:', e

    def do_set_root(self, line):
        print 'Current source root: %s' % self.config_manager.config['src_root']
        root_dir = self.config_manager.root_dir_from_user()
        self.config_manager.config['src_root'] = root_dir
        self.config_manager.save()

    def do_contest(self, line):
        self.run_cmd_suite(self.contest_cmd, line)

    def do_prob(self, line):
        self.run_cmd_suite(self.problem_cmd, line)

    def do_stats(self, line):
        self.run_cmd_suite(self.stats_cmd, line)

class ConfigManager(object):
    def __init__(self):
        if os.path.exists('data/config.json'):
            with open('data/config.json') as f:
                content = f.read()
                try:
                    self.config = json.loads(content)
                except Exception, e:
                    print 'Invalid config file!'
                    self.config = {'src_root': self.root_dir_from_user()}
        else:
            self.config = {'src_root': self.root_dir_from_user()}

        self.save()

    def save(self):
        with open('data/config.json', 'w') as f:
            f.write(json.dumps(self.config))

    def root_dir_from_user(self):
        root_dir = raw_input('Please enter the path of the directory containing your soluton files: ')
        return os.path.expanduser(root_dir.strip())

config_manager = ConfigManager()
store = ProblemStore('data/problems.db')
term = Term(store, config_manager)
term.cmdloop()
