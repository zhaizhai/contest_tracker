import sqlite3
import sys
import uuid

class Problem(object):
    def __init__(self, id, contest_id, number, name, difficulty, status):
        self.id = id
        self.contest_id = contest_id
        self.number, self.name = number, name
        self.difficulty = difficulty
        self.status = status

class Contest(object):
    def __init__(self, id, name, difficulty, status):
        self.id = id
        self.name = name
        self.difficulty = difficulty
        self.status = status

class ProblemStore(object):
    def __init__(self, db_name):
        self.conn = sqlite3.connect(db_name)
        # self.conn = sqlite3.connect(':memory:')
        self._initialize_db(self.conn)

    def _initialize_db(self, conn):
        cursor = conn.cursor()
        cursor.execute('CREATE TABLE IF NOT EXISTS contests (id TEXT, name TEXT, difficulty REAL, status TEXT)')
        cursor.execute('CREATE TABLE IF NOT EXISTS problems (id TEXT, contest_id TEXT, number TEXT, name TEXT, difficulty REAL, status TEXT)')

        cursor.execute('CREATE INDEX IF NOT EXISTS contest_id ON contests (id)')
        cursor.execute('CREATE INDEX IF NOT EXISTS problem_id ON problems (id)')

        # TODO: conn.commit()?

    def _make_contest_id(self):
        return 'c-' + str(uuid.uuid4())

    def _make_problem_id(self):
        return 'p-' + str(uuid.uuid4())

    def _get_single(self, arr):
        if not arr:
            return None
        assert len(arr) < 2, 'expected singleton!'
        return arr[0]

    def export_to_file(self, file_name):
        with open(file_name, 'w') as f:
            for prob in self.query_problems():
                contest = self.get_contest_by_id(prob.contest_id)
                contest_name = (contest.name if contest is not None else 'unknown')
                columns = (contest_name, prob.number, prob.name, prob.status)
                f.write(' '.join(columns) + '\n')

    def import_from_file(self, file_name, prompt=False):
        tot_lines = 0

        with open(file_name) as first_pass:
            for l in first_pass:
                if not l.strip(): continue
                tot_lines += 1

        with open(file_name) as statfile:
            cur_idx = 0
            for l in statfile:
                if not l.strip(): continue

                cur_idx += 1
                mesg = '\rimport line %d of %d...  ' % (cur_idx, tot_lines)
                sys.stdout.write(mesg)
                sys.stdout.flush()
                contest_name, num, name, status = l.strip().split()

                # TODO: prompt user here to verify import
                # TODO: prompt if suspected duplicate

                contest = self.get_contest_by_name(contest_name)
                if contest is None:
                    contest = self.create_contest(contest_name, None) # TODO: difficulty
                    # print 'created contest', contest.name, contest.id

                self.create_problem(contest.id, num, name, None, status)

            mesg = '\rImported %d entries      \n' % tot_lines
            sys.stdout.write(mesg)
            sys.stdout.flush()

    def create_contest(self, name, difficulty, active=False):
        contest_id = self._make_contest_id()
        values = (contest_id, name, difficulty, ('active' if active else 'inactive'))

        c = self.conn.cursor()
        c.execute('INSERT INTO contests VALUES (?, ?, ?, ?)', values)
        self.conn.commit()
        return Contest(*values)

    def create_problem(self, contest_id, number, name, difficulty, status):
        problem_id = self._make_problem_id()

        c = self.conn.cursor()
        values = (problem_id, contest_id, number, name, difficulty, status)
        c.execute('INSERT INTO problems VALUES (?, ?, ?, ?, ?, ?)', values)
        self.conn.commit()
        return Problem(*values)

    def update_contest(self, contest_id, new_status=None):
        if new_status is None:
            return
        c = self.conn.cursor()
        c.execute('UPDATE contests SET status = ? WHERE id = ?', (new_status, contest_id))
        self.conn.commit()

    def update_problem(self, problem_id, new_status=None):
        if new_status is None:
            return
        c = self.conn.cursor()
        c.execute('UPDATE problems SET status = ? WHERE id = ?', (new_status, problem_id))
        self.conn.commit()

    def delete_problem(self, problem):
        pass

    # TODO: cache some of these in memory
    def get_contest_by_name(self, name):
        c = self.conn.cursor()
        c.execute('SELECT * FROM contests WHERE name = ?', (name,))
        values = self._get_single(c.fetchall())
        if values is None:
            return None
        return Contest(*values)

    def get_contest_by_id(self, contest_id):
        c = self.conn.cursor()
        c.execute('SELECT * FROM contests WHERE id = ?', (contest_id,))
        values = self._get_single(c.fetchall())
        if values is None:
            return None
        return Contest(*values)

    def query_contests(self):
        c = self.conn.cursor()
        c.execute('SELECT * FROM contests')
        return [Contest(*values) for values in c.fetchall()]

    def query_problems(self): # TODO: just lists all problems for now
        c = self.conn.cursor()
        c.execute('SELECT * FROM problems')
        # TODO: we could probably make this an iterator to be more
        # efficient...
        return [Problem(*values) for values in c.fetchall()]


