
from collections import defaultdict
from util import Printing, colorize


class ProbCommandSuite(object):
    ALLOWED_STATUSES = ('todo', 'done', 'missing', 'solving')

    def __init__(self, problem_stats, problem_store, progress_tracker):
        self.problem_stats = problem_stats
        self.problem_store = problem_store
        self.progress_tracker = progress_tracker

    def command_map(self):
        return {
            'add': (self.add, 'Add a problem to the list of active contests.'),
            'list': (self.list, 'List active problems.'),
            'suggest': (self.suggest, 'Suggest changes to problem statuses.'),
            'mark': (self.mark, 'Mark a problem\'s status.'),
            }

    def _prob_mesg(self, prob):
        ret = '[%s - %s - %s]' % (prob.number, prob.name, prob.status.upper())
        color = ('green' if prob.status == 'done' else 'red')
        return colorize(color, ret, attributes=('bold',))

    def mark(self, *args):
        if len(args) != 2:
            print 'Usage: prob mark [problem name] [status]'
            return

        prob_name, status = args
        all_probs = self.problem_store.query_problems()
        candidates = [prob for prob in all_probs if prob.name == prob_name]

        if not candidates:
            print 'No problem by that name.'
            return

        if len(candidates) > 1:
            print 'TODO: can\'t handle duplicate problem names yet'
            raise

        status = status.lower()
        if status not in self.ALLOWED_STATUSES:
            print 'Invalid status %s.' % status
            print 'Allowed statuses are %s.' % (', '.join(self.ALLOWED_STATUSES))
            return

        to_mark = candidates[0]
        self.problem_store.update_problem(to_mark.id, new_status=status)

    def suggest(self, *args):
        summary = (len(args) > 0 and args[0] == '-s')

        suggestions = []

        all_probs = self.problem_store.query_problems()
        for prob in all_probs:
            contest = self.problem_store.get_contest_by_id(prob.contest_id)
            suggestion = self.progress_tracker.suggest_status(contest.name, prob)
            if suggestion != prob.status:
                if summary:
                    print 'Status change suggestions are available'
                    return

                print 'Suggest marking %s %s as %s (was %s)' % (contest.name, prob.name, suggestion.upper(), prob.status.upper())
                suggestions.append((prob, suggestion))

        if suggestions:
            accept = raw_input('Accept suggestions? (y/N) ')
            if accept.strip() == 'y':
                for prob, suggestion in suggestions:
                    self.problem_store.update_problem(prob.id, new_status=suggestion)

    def list(self):
        all_probs = self.problem_store.query_problems()
        probs_by_contest = defaultdict(list)
        for prob in all_probs:
            probs_by_contest[prob.contest_id].append(prob)

        active_items = []
        for contest_id, probs in probs_by_contest.iteritems():
            assert len(probs) > 0

            contest = self.problem_store.get_contest_by_id(contest_id)
            assert contest is not None

            if contest.status == 'active':
                active_items.append((contest, probs))

        if not active_items:
            print '\nYou have no active contests with problems available.\n'
            return

        active_items.sort(key=lambda (c, p): self.problem_stats.sort_rank(c))
        for contest, probs in active_items:
            probs.sort(key=lambda p: p.number)

            print Printing.SEPARATOR

            contest_prefix = (contest.name + Printing.PADDING)[:len(Printing.PADDING)]
            contest_prefix = colorize('blue', contest_prefix, attributes=('bold',))

            print contest_prefix, '-', self._prob_mesg(probs[0])
            for prob in probs[1:]:
                print Printing.PADDING, '-', self._prob_mesg(prob)
        print Printing.SEPARATOR

        contest_ids = [c.id for (c, p) in active_items]
        aggregate_stats = self.problem_stats.contest_stats_aggregate(contest_ids)
        total = sum(aggregate_stats.values())
        num_done = aggregate_stats['done']

        assert num_done <= total
        print '[%s%s]' % (colorize('green', 'x' * num_done, attributes=('bold', 'reverse-video')),
                          colorize('red', 'x' * (total - num_done), attributes=('bold',)))
        print Printing.SEPARATOR

    def add(self):
        contest_name = raw_input('\nEnter contest name: ')
        contest = self.problem_store.get_contest_by_name(contest_name)
        if contest is None:
            do_create = raw_input('This contest is not currently in the database. Create a new contest? (y/N)')
            if do_create.strip() != 'y':
                return

            # TODO: no difficulties for now
            difficulty = None
            contest = self.problem_store.create_contest(contest_name, difficulty)

        num = raw_input('Enter problem number: ')
        name = raw_input('Enter problem name: ')

        # TODO: validate these fields

        self.problem_store.create_problem(contest.id, num, name, None, 'todo')
