
from collections import defaultdict
from util import Printing, colorize


class ContestCommandSuite(object):
    def __init__(self, problem_stats, problem_store):
        self.problem_stats = problem_stats
        self.problem_store = problem_store

    def command_map(self):
        return {
            'add': (self.add, 'Add a contest to the list of active contests.'),
            'rm': (self.rm, 'Remove a contest from the list of active contests.'),
            'list': (self.list, 'List all contests.'),
            'mark': (self.mark, 'Update the status of all problems from a contest.'),
            }

    def mark(self, *args):
        if len(args) != 2:
            print 'Usage: contest mark [contest name] [status]'
            return

        contest_name, status = args
        contest = self.problem_store.get_contest_by_name(contest_name)
        all_probs = self.problem_store.query_problems()

        to_mark = [prob for prob in all_probs if prob.contest_id == contest.id]
        for prob in to_mark:
            print 'Setting status of %s to %s (was %s)' % (prob.name, status, prob.status)
            self.problem_store.update_problem(prob.id, new_status=status)


    def list(self):
        all_contests = self.problem_store.query_contests()
        all_contests.sort(key=self.problem_stats.sort_rank)

        for contest in all_contests:
            stats = self.problem_stats.contest_stats(contest.id)
            total = sum(stats.itervalues())

            all_done = (stats['done'] == total)


            done_marker = (colorize('green', '*', attributes=('bold',))
                           if all_done else ' ')
            status_marker = ' '
            if contest.status == 'active':
                status_marker = '[%s]' % done_marker
            else:
                status_marker = ' %s ' % done_marker
            print status_marker + ' ' + contest.name

    def add(self, *args):
        if len(args) == 0:
            print 'Missing contest name'
            return
        contest_name = args[0]
        contest = self.problem_store.get_contest_by_name(contest_name)
        if contest is None:
            print 'No contest by that name'
            return
        self.problem_store.update_contest(contest.id, new_status='active')

    def rm(self, *args):
        if len(args) == 0:
            print 'Missing contest name'
            return
        contest_name = args[0]
        contest = self.problem_store.get_contest_by_name(contest_name)
        if contest is None:
            print 'No contest by that name'
            return
        self.problem_store.update_contest(contest.id, new_status='inactive')


