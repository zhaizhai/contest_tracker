
class Printing(object):
    PADDING = ' ' * 10
    SEPARATOR = '-' * 60
    DEFAULT = '\033[0m'
    COLORS = {
        'red': '\033[31m',
        'green': '\033[32m',
        'blue': '\033[94m',
        'lime': '\033[92m',
        }
    ATTRIBUTES = {
        'bold': '\033[1m',
        'underline': '\033[4m',
        'blink': '\033[5m',
        'reverse-video': '\033[7m',
        'concealed': '\033[8m',
        }


def colorize(color, text, attributes=None):
    attributes = attributes or ()
    attr_prefix = ''.join(Printing.ATTRIBUTES[attr] for attr in attributes)
    return attr_prefix + Printing.COLORS[color] + text + Printing.DEFAULT
