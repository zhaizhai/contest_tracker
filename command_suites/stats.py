
from collections import defaultdict
from util import Printing, colorize


class StatsCommandSuite(object):
    ALLOWED_STATUSES = ('todo', 'done', 'missing', 'solving')

    def __init__(self, problem_stats, problem_store):
        self.problem_stats = problem_stats
        self.problem_store = problem_store

    def command_map(self):
        return {
            '': (self.stats, 'Show aggregate stats.'),
            }

    def stats(self):
        total_probs = 0
        total_done = 0
        contests_finished = 0

        all_contests = self.problem_store.query_contests()
        for contest in all_contests:
            status_map = self.problem_stats.contest_stats(contest.id)
            total = sum(status_map.itervalues())
            done = status_map['done']

            if done == total:
                contests_finished += 1
            total_probs += total
            total_done += done

        print '%d problems from %d contests in the database.' % (total_probs, len(all_contests))
        print '%d problems solved to date.' % total_done
        print '%d contests completed to date.' % contests_finished
